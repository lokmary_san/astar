#include <climits>
#include "Node.h"

Node::Node(int x, int y)
{
  m_coords.SetXY(x, y);
  m_parent = NULL;
  m_f = INT_MAX;
  m_inSet = true;
}

Node::Node(int x, int y, char data)
{
  m_coords.SetXY(x, y);
  m_data = data;
  m_parent = NULL;
  m_f = INT_MAX;
  m_inSet = true;
}

Node::~Node()
{
}

std::ostream& operator<<(std::ostream& stream, Node obj)
{
  stream << "Node: " << obj.GetCoords() 
	 << " G: " << obj.GetG()
	 << " H: " << obj.GetH()
	 << " F: " << obj.GetF();
  return stream;
}


void Node::SetData(char data)
{
  m_data = data;
}

void Node::SetF(int f)
{
  m_f = f;
}

void Node::SetG(int g)
{
  m_g = g;
}

void Node::SetH(int h)
{
  m_h = h;
}

void Node::SetParent(Node* parent)
{
  m_parent = parent;
}

void Node::SetTentativeParent(Node* parent)
{
  m_tentativeParent = parent;
}

void Node::RemovedFromSet()
{
  m_inSet = false;
}
