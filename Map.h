#ifndef MAP_H
#define MAP_H

#include "Point.h"
#include "Node.h"

class Map
{
 public:
  Map();
  ~Map();

  bool LoadMap(const char* mapFileName);
  void PrettyPrint();
  void PrintPath();
  void PrintDijkstras();
  void PrintDigraph();

  unsigned int GetBoardX() { return m_boardX; }
  unsigned int GetBoardY() { return m_boardY; }
  Point& GetStartPoint()   { return m_start;  }
  Point& GetEndPoint()     { return m_end;    }

  Node* GetBoardNode(Point p);
  Node* GetBoardNode(int x, int y);

  bool IsWalkable(Point p);
  bool IsWalkable(int x, int y);

 private:
  // This next line is why people use python
  Node ***m_board; // 2D array of objects

  unsigned int m_boardX;
  unsigned int m_boardY;

  Point m_start;
  Point m_end;
};

#endif
