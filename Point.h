#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <ostream>

class Point
{
 public:
  Point();
  Point(int x, int y);
  
  int GetX() const { return m_xCoord; }
  int GetY() const { return m_yCoord; }
  
  void SetXY(int x, int y);
  
  void SetX(int x);
  void SetY(int y);

  friend bool operator==(Point& lhs, const Point& rhs);
  friend std::ostream& operator<<(std::ostream &stream, Point obj);

 private:
  int m_xCoord;
  int m_yCoord;
};

#endif
