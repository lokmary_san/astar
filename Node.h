#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <ostream>

#include "Point.h"

class Node
{
 public:
  Node(int x, int y);
  Node(int x, int y, char data);
  ~Node();	

  void SetData(char data);
  void SetF(int f);
  void SetG(int g);
  void SetH(int h);
  void SetParent(Node* parent);
  void SetTentativeParent(Node* parent);
  void RemovedFromSet(); // Used by Dijsktra's only

  char   GetData();
  int    GetF();
  int    GetG();
  int    GetH();
  Point& GetCoords();
  Node*  GetParent();
  Node*  GetTentativeParent();

  bool IsInSet(); // Used by Dijkstra's only

  friend std::ostream& operator<<(std::ostream &stream, Node obj);

 private:
  Node*	m_parent;
  Node* m_tentativeParent;
  Point	m_coords;
  
  bool  m_inSet; // Used by Dijkstra's only
  char  m_data;
  int	m_f;
  int   m_g;
  int	m_h;
};

inline
char Node::GetData()
{
  return m_data;
}

inline
int Node::GetF()
{
  return m_f;
}

inline
int Node::GetG()
{
  return m_g;
}

inline
int Node::GetH()
{
  return m_h;
}

inline
Point& Node::GetCoords()
{
  return m_coords;
}

inline
Node* Node::GetParent()
{
  return m_parent;
}

inline
Node* Node::GetTentativeParent()
{
  return m_tentativeParent;
}

inline
bool Node::IsInSet()
{
  return m_inSet;
}


#endif
