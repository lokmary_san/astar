CC := /usr/bin/g++
CP := /bin/cp
MAKE := /usr/bin/make
MKDIR := /bin/mkdir
RM := /bin/rm
TAR := /bin/tar

CFLAGS := -c -Wall -O2
ASTAR_EXE := astar
DIJKSTRAS_EXE := dij

OBJS := Node.o Point.o Map.o

ASTAR_OBJS := astar.o
DIJKSTRAS_OBJS := dij.o

INCLUDES := $(wildcard *.h)

TAR_DIR := astar-final-project
TAR_FILES := Map.h Map.cpp Node.h Node.cpp Point.h Point.cpp \
             astar.cpp dij.cpp Makefile map_gen.py \
             LICENSE.txt README.txt docs maps

ifeq ($(wildcard /usr/bin/dot),)
	MACROS :=
else
	MACROS := -DGRAPH_VIS
endif

.PHONY: all tar rebuild clean cleanemacs distclean check_graphvis

all: $(ASTAR_EXE) $(DIJKSTRAS_EXE) check_graphvis

check_graphvis:
	@( if [ "X$(MACROS)" != "X-DGRAPH_VIS" ]; then \
		echo "The build completed successfully however you will not"; \
		echo "have very pretty output for Dijkstra's algorithm. If"; \
		echo "you would like to see a directed graph representing"; \
		echo "the paths, please install Graphviz (www.graphviz.org)."; \
	fi )

$(ASTAR_EXE): $(ASTAR_OBJS) $(OBJS)
	$(CC) -o "$@" $(ASTAR_OBJS) $(OBJS)

$(DIJKSTRAS_EXE): $(DIJKSTRAS_OBJS) $(OBJS)
	$(CC) -o "$@" $(DIJKSTRAS_OBJS) $(OBJS)

%.o: %.cpp $(INCLUDES)
	$(CC) $(CFLAGS) $(MACROS) "$<" -o "$@"

tar: cleanemacs
	$(MKDIR) -p $(TAR_DIR)
	for s in $(TAR_FILES); do \
            $(CP) -r --parents $$s $(TAR_DIR); \
        done
	$(TAR) -czf $(TAR_DIR).tgz $(TAR_DIR)
	$(RM) -rf $(TAR_DIR)

rebuild:
	$(MAKE) clean
	$(MAKE)

clean:
	$(RM) -f $(ASTAR_OBJS) $(DIJKSTRAS_OBJS) $(OBJS)
	$(RM) -f $(ASTAR_EXE) $(DIJKSTRAS_EXE)

cleanemacs: clean
	$(RM) -f *~


distclean: cleanemacs
	$(RM) -rf $(TAR_DIR) $(TAR_DIR).tgz
