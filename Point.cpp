
#include "Point.h"

Point::Point()
{
  m_xCoord = 0;
  m_yCoord = 0;
}

Point::Point(int x, int y)
{
  m_xCoord = x;
  m_yCoord = y;
}

bool operator==(Point& lhs, const Point& rhs)
{
  if ((lhs.GetX() == rhs.GetX()) && (lhs.GetY() == rhs.GetY()))
    return true;
  return false;
}

std::ostream& operator<<(std::ostream& stream, Point obj)
{
  stream << "(" << obj.GetX() << ", " << obj.GetY() << ")";
  return stream;
}

void Point::SetXY(int x, int y)
{
  m_xCoord = x;
  m_yCoord = y;
}

void Point::SetX(int x)
{
  m_xCoord = x;
}

void Point::SetY(int y)
{
  m_yCoord = y;
}
