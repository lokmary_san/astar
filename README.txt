Sections:

 1 - Compilation
 2 - Running
 3 - Map generation

------------------------------------------------------------------------------
1) Compilation

To compile the code, change into the directory where you untarred the
archive and type:

$ make

Makefile targets available:
  - all (default)
    - make both astar and dij
  - astar
    - Compiles the A* program
  - dij
    - Compile the Dijkstra program
  - clean
    - cleans created executables and object files
  - cleanemacs
    - calls the clean target then additionally removes temporary emacs files
  - distclean
    - calls the cleanemacs target then additionally removes the tar archive
  - rebuild
    - calls 'make clean' then 'make'
  - tar
    - create a tar package of everything needed to rebuild the source

Note:
For the Dijkstra implementation, it has the ability to output a format
recognized by the Graphviz program (www.graphviz.org).  If the 'dot' binary
is located in the /usr/bin directory, the program will output in that format,
otherwise it will output in an ascii-based format.  Both of these are
described in the next section.

------------------------------------------------------------------------------

2) Running

2.1) astar

To run astar, use the following command:

$ ./astar maps/mapXX.map

This will parse the map and find the shortest path from the start node to the
end node. It will output two maps. the first is the original map read in. The
second will overlay a series of 'O' characters that mark the best path found.

2.2) dij

To run dij, use the following command:

$ ./dij maps/mapXX.map

This will parse the map and find the shortest path from the source node to all
other nodes in the map.  The output will be different based upon whether or not
the Graphviz program is available on the system.

If Graphviz *is* installed, the output will be the format that can be read by
the 'dot' binary. To create an appropriate graphic, use the following commands:

$ ./dij maps/mapXX.map > my.dot
$ dot -Tpng -o mygraph.png my.dot

This can then be viewed using your favorite image viewer, for example Firefox:

$ firefox ./mygraph.png &

The image displayed will have the single source node up top and the path taken
to each node in the map.  The verticies will be labeled with the position of
the node visited and the edges will contain the cost to move to that node as
well as the total overall cost directly adjacent in parentheses.

If Graphviz is *not* installed, the output will consist of the original map
contents with the cost of getting to each point displayed in parentheses
immediately next to each map character.

------------------------------------------------------------------------------

3) Map Generation

A python program to generate map files is included. To use, run the following:

$ ./map_gen.py -x # -y # [-s "<chars>"]

For its basic usage, X and Y dimensions must be provided. Additionally,
obstacles can be specified with the -s option. The characters in the given
string will be placed a random number of times throughout the map. Currently,
the only supported obstacles are:

  - W: An impassable wall
  - M: Mud terrain that increases the cost of a move by a factor of 1.3

To save a map file, use the following example command:

$ ./map_gen.py -x 5 -y 8 -s "W" > maps/mapXX.py

This will create a 5x8 map with wall obstacles. Also, start and end points
will be randomly placed on the map.

